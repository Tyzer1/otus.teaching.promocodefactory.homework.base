﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class PromoCodeDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }

        public PromoCodeDbContext(DbContextOptions<PromoCodeDbContext> dbContextOptions)
            : base(dbContextOptions)
        {
            Database.EnsureCreated();
        }

    }
}
