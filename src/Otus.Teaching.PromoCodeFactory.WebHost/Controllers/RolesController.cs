﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController
        : ControllerBase
    {
        private readonly IRepository<Role> _rolesRepository;

        public RolesController(IRepository<Role> rolesRepository)
        {
            _rolesRepository = rolesRepository;
        }
        
        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(Role), StatusCodes.Status200OK)]
        public async Task<List<RoleItemResponse>> GetRolesAsync()
        {
            var roles = await _rolesRepository.GetAllAsync();

            var rolesModelList = roles.Select(x => 
                new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList();

            return rolesModelList;
        }

        /// <summary>
        /// Получить роль сотрудника по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(Role), StatusCodes.Status200OK)]
        public async Task<ActionResult<RoleItemResponse>> GetRoleByIdAsync(Guid id)
        {
            var role = await _rolesRepository.GetByIdAsync(id);

            if (role == null)
                return NotFound();

            return GetRoleResponce(role);
        }

        /// <summary>
        /// Создать новую роль для сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<ActionResult<RoleItemResponse>> Create(Role role)
        {
            var addedRole = await _rolesRepository.AddAsync(role);

            if (addedRole == null)
                return BadRequest();

            return CreatedAtAction("GetRoleByIdAsync", new { id = role.Id }, GetRoleResponce(role));
        }

        /// <summary>
        /// Удалить роль
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Delete(Guid id)
        {
            var role = await _rolesRepository.GetByIdAsync(id);

            if (role == null)
                return NotFound();

            await _rolesRepository.DeleteAsync(id);

            return NoContent();
        }

        /// <summary>
        /// Обновить информацию о роли сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(Role), StatusCodes.Status200OK)]
        public async Task<ActionResult<RoleItemResponse>> Update(Guid id, Role newRole)
        {
            var role = await _rolesRepository.GetByIdAsync(id);

            if (role == null)
                return NotFound();

            var updatedEmployee = await _rolesRepository.UpdateAsync(id, newRole);

            return GetRoleResponce(updatedEmployee);
        }

        private RoleItemResponse GetRoleResponce(Role role)
        {
            var roleResponce = new RoleItemResponse()
            {
                Id = role.Id,
                Name = role.Name,
                Description = role.Description
            };

            return roleResponce;
        }
    }
}